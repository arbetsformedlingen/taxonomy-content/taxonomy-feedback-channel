# Feedback avseende innehållet i Taxonomy

Under Plan/Issue boards till vänster finns en ärendetavla för feedback gällande innehållet i Taxonomy. 

Se https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-feedback-channel/-/boards/4013239
